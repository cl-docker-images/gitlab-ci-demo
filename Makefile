.PHONY: test build

ifeq ($(LISP), abcl)
test:
	$(LISP) --eval '(print (multiple-value-list (lisp-implementation-version)))' --eval '(quit)'
else
test:
	$(LISP) --version
endif

build:
	echo "Built with $$LISP" > some-file-name
